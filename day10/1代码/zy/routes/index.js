var express = require('express');
var router = express.Router();
var svgCaptcha = require('svg-captcha'); // 引入
const path = require('path');
const fs = require('fs');
const timeStamp = require('time-stamp');

router.get('/', function (req, res, next) {
  res.send('首页');
});

// 渲染登录页面
router.get('/login', function (req, res, next) {
  res.render('login');
});

// 渲染注册页面
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 登录功能
router.post('/login', function (req, res, next) {
  res.send('登录');
});

// 注册功能
router.post('/register', function (req, res, next) {
  // console.log(req.body);
  // 1、获取用户输入信息
  let { username, password, password2, code } = req.body;

  // 2、进行一些判断
  if (!username || !password || !password2 || !code) {
    next('必须填定');
    return;
  }
  if (password !== password2) {
    next('两次密码必须一致');
    return;
  }
  if (code.toUpperCase() !== req.session.captcha.toUpperCase()) {
    next('验证码错误');
    return;
  }

  // 3、读取本地信息
  let filePath = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(filePath, 'utf-8');
  data = JSON.parse(data);

  // 4、逻辑判断
  let index = data.findIndex((item) => item.username === username);
  if (index === -1) {
    // 可以注册
    data.push({
      username,
      password,
      time: timeStamp('YYYY年MM月DD日 HH:mm:ss'),
    });
    fs.writeFileSync(filePath, JSON.stringify(data));
    res.send('<h2>注册成功</h2>');
  } else {
    // 存在
    next('用户名已占用');
  }
});

// 验证码路由，访问/captcha时，可以得到验证码图片
router.get('/captcha', function (req, res) {
  // 可以设置参数
  var captcha = svgCaptcha.create({
    size: 4, // 验证码长度，默认4个
    ignoreChars: '0o1i', // 忽略哪些字符串
    noise: 3, // 干扰线的条数
    background: '#999999', // 背景色
    // color:true // 文字是否加颜色（没有背景色时才起效）
  });

  // 将验证码文本保存在session中
  req.session.captcha = captcha.text;

  res.type('svg'); // svg格式的文件类型，它是一个图片
  res.status(200).send(captcha.data); // data属性就是前端要用的图片数据
});

module.exports = router;
