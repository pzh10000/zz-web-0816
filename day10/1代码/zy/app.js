var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

var indexRouter = require('./routes/index');

var app = express();

const cookieSession = require('cookie-session'); // 引入cookieSession中间件

// 使用cookieSession中间件
app.use(
  cookieSession({
    name: 'sessionids', // sessionid的名称，默认可以省略
    keys: ['gffdgdsgfd', 'secggfdgdret2', 'ggdfgfdgd'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

// ejs
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// post参数
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// cookie
app.use(cookieParser());
// 开启静态资源
app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(path.join(__dirname, './static')));

app.use('/', indexRouter);

app.use(function (req, res, next) {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

app.use(function (err, req, res, next) {
  res.status(500);
  // res.send({
  //   code: 500,
  //   msg: err,
  // });
  res.render('message', { err });
});

module.exports = app;
