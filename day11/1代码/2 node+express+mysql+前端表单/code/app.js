var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use(function (req, res, next) {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

app.use(function (err, req, res, next) {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});

module.exports = app;
