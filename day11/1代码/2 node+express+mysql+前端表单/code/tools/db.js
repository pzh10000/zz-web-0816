const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'xiaou',
});

function Query(sql = '') {
  return new Promise((resolve, reject) => {
    connectObj.query(sql, (err, result) => {
      // 如果请求成功，返回的数组有两项，第一项为null，数据在第二项
      // 如果请求失败，返回的数组有一项，第一项就是失败的对象
      if (!err) {
        resolve([null, result]);
      } else {
        resolve([err]);
      }
    });
  });
}

module.exports = Query;
