var express = require('express');
var router = express.Router();
const Query = require('../tools/db');

router.get('/', function (req, res, next) {
  res.send('index');
});

// 渲染注册页面
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 渲染登录页面
router.get('/login', function (req, res, next) {
  res.render('login');
});

// 实现注册功能
router.post('/register', async function (req, res, next) {
  // 获取用户信息
  let { username, password } = req.body;

  // 非空判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 查找数据库，是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username='${username}'`;

  let [err1, result1] = await Query(sql1);
  // console.log(err1, result1);
  if (err1) {
    next('服务器内部错误');
    return;
  }
  if (result1.length) {
    next('用户名已占用');
    return;
  }

  // 可以注册，就数据中插入一条数据
  let sql2 = `INSERT INTO user (username, password) VALUES ('${username}', '${password}')`;
  let [err2, result2] = await Query(sql2);
  if (err2) {
    next('服务器内部错误');
    return;
  }
  if (result2.affectedRows) {
    res.send({
      code: 200,
      msg: '注册功能',
    });
  } else {
    next('注册失败');
  }
});

// 实现登录功能
router.post('/login', async function (req, res, next) {
  // 获取用户信息
  let { username, password } = req.body;

  // 非空判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 查询数据库
  let sql = `SELECT * FROM user WHERE username='${username}' AND password = '${password}'`;
  let [err, result] = await Query(sql);
  if (err) {
    next('服务器内部错误');
    return;
  }

  // 如果返回的数组有值，则是登录成功
  if (result.length) {
    res.send({
      code: 200,
      msg: '登录成功',
    });
  } else {
    next('用户名和密码不匹配');
  }
});

module.exports = router;
