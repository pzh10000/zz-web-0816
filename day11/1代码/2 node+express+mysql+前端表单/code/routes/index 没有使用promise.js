var express = require('express');
var router = express.Router();

// 连接数据库
const mysql = require('mysql');
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'xiaou',
});

router.get('/', function (req, res, next) {
  res.send('index');
});

// 渲染注册页面
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 渲染登录页面
router.get('/login', function (req, res, next) {
  res.render('login');
});

// 实现注册功能
router.post('/register', function (req, res, next) {
  // 获取用户信息
  let { username, password } = req.body;

  // 非空判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 查找数据库，是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username='${username}'`;
  connectObj.query(sql1, (err, result) => {
    if (err) {
      next('服务器内部错误');
      return;
    }

    if (result.length) {
      next('用户名已占用');
      return;
    }

    // 可以注册，就数据中插入一条数据
    let sql2 = `INSERT INTO user (username, password) VALUES ('${username}', '${password}')`;
    connectObj.query(sql2, (err, result) => {
      if (err) {
        next('服务器内部错误');
        return;
      }

      if (result.affectedRows) {
        res.send({
          code: 200,
          msg: '注册成功',
        });
      }
    });
  });

  
});

// 实现登录功能
router.post('/login', function (req, res, next) {
  // 获取用户信息
  let { username, password } = req.body;

  // 非空判断
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 查询数据库
  let sql = `SELECT * FROM user WHERE username='${username}' AND password = '${password}'`;
  connectObj.query(sql, (err, result) => {
    if (err) {
      next('服务器内部错误');
      return;
    }

    // 如果返回的数组有值，则是登录成功
    if (result.length) {
      res.send({
        code: 200,
        msg: '登录成功',
      });
    } else {
      next('用户名和密码不匹配');
    }
  });
});

module.exports = router;
