/*
Navicat MySQL Data Transfer

Source Server         : 王二小
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : xiaou

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2022-08-30 14:34:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'zs', '123');
INSERT INTO `user` VALUES ('2', 'abc', '123');
