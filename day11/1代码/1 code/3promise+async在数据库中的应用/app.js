// 1、下载  npm i mysql

// 2、引入
const mysql = require('mysql');

// 3、创建mysql数据库的配置连接
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

function Query(sql = '') {
  return new Promise((resolve, reject) => {
    connectObj.query(sql, (err, result) => {
      // 如果请求成功，返回的数组有两项，第一项为null，数据在第二项
      // 如果请求失败，返回的数组有一项，第一项就是失败的对象
      if (!err) {
        resolve([null, result]);
      } else {
        resolve([err]);
      }
    });
  });
}

let sql = `SELECT * FROM student`;
async function auto() {
  let [err, result] = await Query(sql);
  // console.log(err);
  // console.log(result);

  if (err) {
    console.log('请求错误');
    return;
  }
  console.log(result);
}

auto();
