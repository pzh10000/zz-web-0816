const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

let sql = `DELETE FROM student WHERE id=9`; // 我们在navicat中测试好
connectObj.query(sql, (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
  if (result.affectedRows) {
    // 受影响的行如果存在，则删除成功，否则就是删除失败
    console.log('删除成功');
  } else {
    console.log('删除失败');
  }
});
