const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

let sql = `UPDATE student SET uname='王二小' WHERE id=8`; // 我们在navicat中测试好
connectObj.query(sql, (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result); // 结果
  if (result.affectedRows) {
    // 受影响的行如果存在，则更新成功，否则就是更新失败
    console.log('更新成功');
  }
});
