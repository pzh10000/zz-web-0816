const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

let sql = `SELECT * FROM teacher WHERE username='付老师1'`; // 我们在navicat中测试好
connectObj.query(sql, (err, result) => {
  if (err) {
    console.log(err);
    return;
  }

  // 返回的一定是一个数组，没有值返回一个空数组，看数组的长度即可知道是否查成功
  console.log(result); // 结果
});
