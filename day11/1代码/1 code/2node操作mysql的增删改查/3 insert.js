const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

let sql = `INSERT INTO teacher (idcard,username,sex,age) VALUES ('a123','小二二',0,18)`; // 我们在navicat中测试好
connectObj.query(sql, (err, result) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result); // 结果
  if (result.affectedRows) {
    // 受影响的行如果存在，则添加成功，否则就是添加失败
    console.log('添加成功');
  } else {
    console.log('添加失败');
  }
});
