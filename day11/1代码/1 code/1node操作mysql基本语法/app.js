// 1、下载  npm i mysql

// 2、引入
const mysql = require('mysql');

// 3、创建mysql数据库的配置连接
let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'stu',
});

// 4、使用connectObj.query()操作数据库
let sql = `SELECT * FROM student`; // 我们在navicat中测试好
connectObj.query(sql, (err, result) => {
  console.log(err); // 错误对象
  console.log(result); // 结果
});
