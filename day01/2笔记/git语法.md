```sh
// 全局配置
git config --global user.name pzh
git config --global user.email pzh_10000@163.com

// 检查是否配置成功
git config --list

// 初始化
git init
// 把工作区 添加到 暂存区
git add 文件名
// 把暂存区添加到仓库区
git commit -m 提交说明

// 看仓库中文件的状态
git status

// 查看提交的日志记录（版本号）
git log   以完整形式输出
git log --oneline  以简短形式输出

// 回退版本
git reset --hard 版本号

// 查看所有的版本（简短形式展示）
git reflog

// 查看当前git管理的项目中有哪些分支
git branch

// 创建一个分支
git branch 分支名

// 切换分支
git checkout 分支名

// 合并分支
git merge 分支名

// 设置远程仓库别名
git remote add 别名 远程仓库地址

// 查看远程仓库的别名
git remote -v

// 本地文件推送到远程仓库
git push 远程仓库 分支名称
git push 远程仓库的别名 分支名称
git push -u 远程仓库的别名 分支名称
git push : 你想只用git push就实现上传（推送）那么必须执行一次 加一个-u 参数的推送

// 克隆分支到本地
git clone 地址

// 同步分支到本地
git pull 地址

ssh
第一步：执行命令
ssh-keygen -t rsa -b 4096 -C "git全局配置的邮箱"
第二步：将公钥加在仓库
第三步：检验：ssh -T git@gitee.com。需要在本地的用户文件夹下.ssh生成一个known_hosts文件
第四步：将本地文件提交给远程仓库（注意，远程仓库地址不能用https了）
```

