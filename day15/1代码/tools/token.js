const jwt = require('jsonwebtoken');
const { secret } = require('../config');

// 设置token
// 参数：对象，时间天
// 返回token字符串
function setToken(obj, t = 7) {
  return jwt.sign(obj, secret, { expiresIn: t * 24 * 60 * 60 });
}

// 解析token
// 参数：token字符串
// 成功返回对象，失败返回false
function getToken(token) {
  try {
    return jwt.verify(token, secret);
  } catch (error) {
    return false;
  }
}

module.exports = {
  setToken,
  getToken,
};
