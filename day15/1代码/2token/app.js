const jwt = require('jsonwebtoken');
let secret = 'lkj;lk;;kjkj;';

// 设置token
// jwt.sign(data, 密钥 [,option])   加密之后，会返回一个token字符串

// 验证token
// jwt.verify( token, 密钥 )        接收token字符串，如果解密成功，返回一个对象，解密失败报错

let obj = {
  user: 'zs',
  age: 3,
  sex: '男',
};

// 生成token，传入对象、密钥、过期时间
let str = jwt.sign(obj, secret, { expiresIn: 24*60*60 });
console.log(str); // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoienMiLCJhZ2UiOjMsInNleCI6IueUtyIsImlhdCI6MTY2MjM2MjgxNiwiZXhwIjoxNjYyNDQ5MjE2fQ.HcSLK3lPIqRa7QunslC6tccJwxoPVdd8nriealCTFNU

// 验证token，传入token字符串和密钥，如果成功，验证通过，否则验证失败
let s = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoienMiLCJhZ2UiOjMsInNleCI6IueUtyIsImlhdCI6MTY2MjM2MjgxNiwiZXhwIjoxNjYyNDQ5MjE2fQ.HcSLK3lPIqRa7QunslC6tccJwxoPVdd8nriealCTFNU';
console.log(jwt.verify(s, secret));