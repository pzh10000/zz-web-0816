const fs = require('fs');

// 取得数据
let data = fs.readFileSync('./persons.json', 'utf-8'); // 字符串
data = JSON.parse(data); // 转成数组对象

let user = { username: 'admin', password: '12345' };

let index = data.findIndex((item) => item.username == user.username);
// console.log(index);

if (index == -1) {
  // 可以注册，将数据写入json文件中
  data.push(user);
  fs.writeFileSync('./persons.json', JSON.stringify(data));
  console.log('注册成功');
} else {
  // 用户已占用
  console.log('此用户名太火了，换一个吧');
}
