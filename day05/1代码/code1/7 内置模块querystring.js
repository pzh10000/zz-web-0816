let qs = require('querystring');

let str = 'name=zs&age=3'; // 查询字符串

// qs.parse(查询字符串); 将查询字符串解析成对象
let obj = qs.parse(str);
console.log(obj); // { name: 'zs', age: '3' }

// qs.stringify(对象); 将对象转换成查询字符串
let s = qs.stringify(obj);
console.log(s); // name=zs&age=3
