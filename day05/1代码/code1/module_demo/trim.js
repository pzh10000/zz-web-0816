// 去除字符串左右空格
function trim(str) {
  let re = /^\s+|\s+$/g;
  return str.replace(re, '');
}

// 去左空格
trim.leftTrim = function (str) {
  let re = /^\s+/;
  return str.replace(re, '');
};

// 去右空格
trim.rightTrim = function (str) {
  let re = /\s+$/;
  return str.replace(re, '');
};

module.exports = trim; // 导出
