// 定义数据
let name = 'zs';
let age = 3;
let fn = function () {
  console.log('前端开发');
};

// 使用exports暴露数据
exports.name = name;
exports.age = age;
exports.fn = fn;
