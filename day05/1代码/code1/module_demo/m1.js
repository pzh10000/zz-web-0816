// 定义数据
let name = 'zs';
let age = 3;
let fn = function () {
  console.log('前端开发');
};

// 使用 module.exports 暴露数据
module.exports = {
  name,
  age,
  fn,
};
