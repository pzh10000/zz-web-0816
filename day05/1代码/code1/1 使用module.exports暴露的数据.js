// let obj = require('./module_demo/m1.js');
// console.log(obj); // { name: 'zs', age: 3, fn: [Function: fn] }
// console.log(obj.name);
// console.log(obj.age);
// obj.fn();

// -----------------------
// 解构
let { name, age, fn } = require('./module_demo/m1');

console.log(name);
console.log(age);
fn();
