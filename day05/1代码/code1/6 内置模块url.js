const myurl = 'http://www.ujiuye.com:8080/a/b/c?name=zs&age=3#abc';

let urlObj = new URL(myurl); // 返回一个对象
// console.log(urlObj);
// console.log(urlObj.searchParams); // map数据结构，所有的参
console.log(urlObj.searchParams.get('name')); // zs
console.log(urlObj.searchParams.get('age')); // 3

console.log(urlObj.pathname); // /a/b/c   文件相对于根目录的位置