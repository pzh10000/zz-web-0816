const path = require('path');
const fs = require('fs');

const filepath = 'a/b/c/d/a.html';

// ----------------------------
// path.parse(路径)：解析路径为对象
let obj = path.parse(filepath);
console.log(obj); // { root: '', dir: 'a/b/c/d', base: 'a.html', ext: '.html', name: 'a' }

// ----------------------------
// - path.basename(路径)：获取路径中的文件名
// - path.extname(路径)：获取路径中文件的后缀名
console.log(path.basename(filepath)); // a.html
console.log(path.extname(filepath)); // .html


// ----------------------------
// path.join(路径的参数列表)：将路径参数列表拼接为绝对地址(重点)
let url = path.join(__dirname, './module_demo/ab.txt');
console.log(url); // 拼出一个完整的地址  D:\0816\day05\1代码\code1\module_demo\ab.txt
console.log(fs.readFileSync(url, 'utf-8')); // hello
