let trim = require('./module_demo/trim');

// console.log(trim); // 它是一个函数，它下面有去左边和去右边的

let str = '   小王吧     ';

console.log('(' + str + ')');
console.log('(' + trim(str) + ')');
console.log('(' + trim.leftTrim(str) + ')');
console.log('(' + trim.rightTrim(str) + ')');
