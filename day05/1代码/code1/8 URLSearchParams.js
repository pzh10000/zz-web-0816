let str = 'name=zs&age=3';

let obj = new URLSearchParams(str);
console.log(obj); // map数据结构  { 'name' => 'zs', 'age' => '3' }

console.log(obj.get('name')); // zs
console.log(obj.get('age')); // 3

console.log(obj.toString()); // name=zs&age=3
