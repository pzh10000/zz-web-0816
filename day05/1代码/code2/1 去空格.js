let trim = require('trim');

// console.log(trim);

let str = '   小王吧     ';

console.log('(' + str + ')');
console.log('(' + trim(str) + ')');
console.log('(' + trim.left(str) + ')');
console.log('(' + trim.right(str) + ')');
