let fs = require('fs');

// fs.readdirSync(path)
// 返回的是该文件夹下的文件内容，是一个数组

let arr = fs.readdirSync('./demo1');
console.log(arr); // [ 'aa', 'ab.txt', 'bb', 'c.txt', 'd.txt' ]
