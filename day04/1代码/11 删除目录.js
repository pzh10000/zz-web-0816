let fs = require('fs');

// fs.rmdirSync( path[,options] )

// fs.rmdirSync('./demo2/aa'); // 删除一层

// fs.rmdirSync('./a', { recursive: true }); // 递归删除，以后有可能替换
fs.rmSync('./a', { recursive: true }); // 以后用这个
