// console.log(global); // node中的顶层对象

// setTimeout(() => {
//   console.log('王二小');
// }, 2000);

// __dirname：当前文件所在的绝对地址目录
// __filename：当前文件所在的绝对地址目录（包含当前的文件名称）

console.log(__dirname); // 当前文件所在的绝对地址目录
console.log(__filename); // 当前文件所在的绝对地址目录（包含当前的文件名称）
