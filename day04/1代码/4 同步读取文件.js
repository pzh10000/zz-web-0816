let fs = require('fs');

// 不加第二个参数，默认返回16进制，用toString()转换一下即可
// let str = fs.readFileSync('./demo1/ab.txt');
// console.log(str.toString());

// 加第二个参数，可以直接写字符串，也可以写成对象
// let str = fs.readFileSync('./demo1/ab.txt', 'utf-8');
// let str = fs.readFileSync('./demo1/ab.txt', { encoding: 'utf-8' });
// console.log(str);

// ---------------------
// 同步代码如何处理错误，用try-catch
console.log(1);
try {
  // 可能有错误的代码
  let str = fs.readFileSync('./demo1/ab.txt', 'utf-8');
  console.log(str);
} catch (err) {
  // 如果上面有错误，就捕获错误，后代码继续
  console.log(err);
}
console.log(2);
