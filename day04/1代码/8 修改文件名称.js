let fs = require('fs');

// fs.renameSync( oldpath, newpath )
// 同时也是剪切功能（如果路径不一样，就是剪切，如果路径一样，就是重命名）

// 同文件夹下，则改名
// fs.renameSync('./demo1/abc.txt', './demo1/ddd.txt');

// 不同文件夹，则移动文件并改名
fs.renameSync('./demo1/ddd.txt', './demo2/abc.txt');
