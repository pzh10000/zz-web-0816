const fs = require('fs');

// 创建目录
// fs.mkdirSync( path[,options] )

// fs.mkdirSync('./demo2/aa'); // 只创建一层
fs.mkdirSync('./a/b/c/d', { recursive: true }); // 递归创建
