let fs = require('fs');

// console.log(123);

// fs操作文件时，如果用的相对路径，是相对于命令行的路径
// let str = fs.readFileSync('./demo1/ab.txt', 'utf-8');
// console.log(str);

// ---------------------
// 一般情况，用fs操作文件，必须用绝对地址
// console.log(__dirname); // D:\0816\day04\1代码
// console.log(__dirname + '\\demo1\\ab.txt'); // D:\0816\day04\1代码\demo1\ab.txt
let str = fs.readFileSync(__dirname + '\\demo1\\ab.txt', 'utf-8');
console.log(str);
