let fs = require('fs');

// 语法：let stat = fs.statSync( path )

let stat = fs.statSync('./demo1/ab.txt');
// console.log(stat); // 读取文件信息，返回一个对象

// 判断是否是文件或文件夹
console.log(stat.isFile()); // true
console.log(stat.isDirectory()); // false
