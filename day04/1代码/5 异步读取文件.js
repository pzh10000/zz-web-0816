let fs = require('fs');

// 异步读取
// fs.readFile( path [,options], callback )

// 没有第二个参数
// fs.readFile('./demo/ab.txt', function (err, result) {
//   if (err) {
//     console.log(err);
//     return;
//   }
//   console.log(result.toString());
// });

// 有第二个参数
fs.readFile('./demo1/ab.txt', 'utf8', function (err, result) {
  if (err) {
    console.log(err);
    return;
  }
  console.log(result);
});
