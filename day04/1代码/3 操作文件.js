let fs = require('fs'); // 引入fs模块，node内置模块，返回一个对象，对象下面有很多的方法
// console.log(fs);

// 打开文件-> 对文件进行操作-> 关闭文件（释放内存）
// 这些方法太麻烦，后面有简单方法（这里了解即可）

// 1、打开文件，返回文件引用
// 语法：fs.openSync(path[, flags])
// path：打开文件的路径
// 打开模式：只读r（默认）、写入w、追加a

let file = fs.openSync('./demo1/ab.txt', 'a'); // 返回文件引用
// console.log(file);

// ------------------

// 2、对文件进行操作（写入内容）
// 语法：fs.writeSync( 文件的引用, 内容 )
fs.writeSync(file, 'hello');

// -------------------

// 3、关闭文件（释放内存）
// 语法：fs.closeSync(文件的引用)
fs.closeSync(file);
