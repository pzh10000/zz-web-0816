const http = require('http');

let app = http.createServer();

app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  // 从请求里面获取的参数
  console.log(req.method); // 请求方法
  console.log(req.url); // 请求地址

  // 设置响应头信息，一定要在响应的前面设置
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' });

  res.end('<h1>你好，小王</h1>');
});
