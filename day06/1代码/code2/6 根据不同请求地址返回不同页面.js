const http = require('http');
const fs = require('fs');
const path = require('path');

let app = http.createServer();

app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  // 设置响应头信息，一定要在响应的前面设置
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' });

  let url = req.url; // url地址

  // 如果访问的是/login，我就返回登录页面，如果访问的是/register，我就返回注册页面
  if (url.startsWith('/login')) {
    let url = path.join(__dirname, './views/login.html');
    let data = fs.readFileSync(url); // 读出来的是二进制的字符串

    res.end(data);
  } else if (url.startsWith('/register')) {
    let url = path.join(__dirname, './views/register.html');
    let data = fs.readFileSync(url); // 读出来的是二进制的字符串

    res.end(data);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf-8' });
    res.end('404');
  }
});
