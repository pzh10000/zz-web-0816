const http = require('http');

let app = http.createServer();

app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.on('request', (req, res) => {
  // 设置响应头信息，一定要在响应的前面设置
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf-8' });

  let url = req.url; // url地址
  // console.log(url);

  // 如果访问的是/login，我就返回登录，如果访问的是register，我就返回注册
  if (url.startsWith('/login')) {
    res.end('<h1>登录</h1>');
  } else if (url.startsWith('/register')) {
    res.end('<h1>注册</h1>');
  } else {
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf-8' });
    res.end('404');
  }
});
