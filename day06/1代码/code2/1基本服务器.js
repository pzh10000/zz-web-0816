// 1. 引入http模块，它是一个对象，下面有一个createServer方法，用于创建服务器，返回服务器引用
const http = require('http');

// console.log(http); // 它是一个对象

// 2. 调用createServer，创建一个服务器，它下面有一个listen方法，用于兼听端口
let app = http.createServer(); // app就是服务器

// 3. 调用listen方法，开放端口，一般使用3000，不要使用小的，因为电脑上的一些内置软件和服务可能已经占用了，listen接收两个参数，第一个为端口号，第二个参数为回调函数，代表监听成功的回调
app.listen(3000, () => {
  console.log('http://localhost:3000  端口开启成功');
});

// 4. 服务器要监听客户的请求，当用户向这个3000端口发起请求，就会触发on绑定的request的事件函数
// 该函数有两个参数，请求和响应，有请求进来，就必须有一个响应回去，用end结束响应
// 注意：修改了代码必须使用node重新运行

let n = 0;
app.on('request', (req, res) => {
  n++;
  console.log('有' + n + '次访问了');
  res.end('hello, ' + n); // 服务端响应用户
});
