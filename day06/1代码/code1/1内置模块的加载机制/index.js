// const fs = require('fs'); // 即便node_modules中有第三方fs模块，它也是找内置模块
// console.log(fs);

const fs = require('fs1'); // 当require引入不以.或..开头，它的加载顺序是：先找内置，如果没有，再找node_modules下的第三方，如果没有，就报错
console.log(fs);
