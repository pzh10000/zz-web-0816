const fs = require('fs');
const path = require('path');

function readdir(dir) {
  let arr = fs.readdirSync(dir); // [ '444.txt', 'a', 'b' ]
  arr.forEach((item) => {
    let fileUrl = path.join(dir, item);
    // console.log(fileUrl); // demo下同的每个文件的地址

    let stat = fs.statSync(fileUrl); // 获取每一个文件的详细信息
    if (stat.isFile()) {
      console.log(fileUrl); // 是文件，就打印
    } else {
      readdir(fileUrl); // 是文件夹，则递归调用
    }
  });
}

let dir = path.join(__dirname, './demo');
// console.log(dir);
readdir(dir);
