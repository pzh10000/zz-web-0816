const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');

// 渲染页面
router.get('/register', (req, res) => {
  let url = path.join(__dirname, '../views/register.html');
  res.sendFile(url);
});

// 登录功能
router.get('/doregister', (req, res) => {
  // 1、读取用户输入
  let { username, password } = req.query; // 取得用户的输入信息
  // 非空的判断
  if (!username || !password) {
    res.send('请输入用户名和密码');
    return;
  }

  // 2、获取本地json中的数据
  let url = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(url, 'utf-8');
  data = JSON.parse(data);

  // 比对用户信息和本地信息
  let index = data.findIndex((item) => item.username === username);
  if (index === -1) {
    // 可以注册
    data.push({ username, password });
    fs.writeFileSync(url, JSON.stringify(data));
    res.send('注册成功');
  } else {
    // 用户名已占用
    res.send('用户名已占用');
  }
});

module.exports = router;
