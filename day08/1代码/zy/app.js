const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let login = require('./router/login'); // 登录
let register = require('./router/register'); // 注册
app.use(login);
app.use(register);
