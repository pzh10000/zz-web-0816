const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 开放静态资源：除了html页面以外的，其它的都是静态资源，这些资源放在服务器上，需要开放，前端才可以访问到

// 没有添加前缀，访问：/img/erha2.jpg
// app.use(express.static(path.join(__dirname, './static')));

// 添加前缀（通常添加前缀），通常添加上静态资源的文件夹名，访问：/static/img/erha2.jpg
app.use('/static', express.static(path.join(__dirname, './static')));

app.get('/login', (req, res) => {
  let filePath = path.join(__dirname, './views/login.html');
  res.sendFile(filePath);
});
