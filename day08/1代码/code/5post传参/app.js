const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 设置获取post参数的中间件，不论何种方式，数据都在req.body中
// 注意：成熟的后端，两种中间件都要配置上
app.use(express.urlencoded({ extended: false })); // 接收post的查询字符串参数
app.use(express.json()); // 接收json对象参数

// 1、获取查询字符串参数
app.post('/demo1', (req, res) => {
  console.log(req.body); // { name: 'zs', pass: '123' }
  res.send('查询字符串参数');
});

// 2、获取json对象参数
app.post('/demo2', (req, res) => {
  console.log(req.body); // { username: 'zs', password: 123 }
  res.send('json对象参数');
});
