const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let timeStamp = require('time-stamp'); // 引入时间模块

// - 在所有的路由前面定义中间件，只有错误中间件放在所有路由的后面
// - 使用中间件的语法形式和定义路由是相似的
// - 无论什么请求都会经过中间件（都会匹配到中间件）
// - 中间件及路由的req是共享的一个对象，因此在中间件中的一些公共数据可以绑定到 req 对象上，供具体的路由中使用
// - 中间件必须要next往下走，匹配最终路由

// 只关心注册时间，不关心登录，添加前缀
app.use('/register', (req, res, next) => {
  let time = timeStamp('YYYY年MM月DD日 HH:mm:ss');
  req.time = time; // 给req添加了一个自定义的属性，这个自定义属性，在路由中可以获取
  next();
});

app.get('/login', (req, res) => {
  res.send('中间件 get-login---' + req.time);
});

app.post('/login', (req, res) => {
  res.send('中间件 post-login---' + req.time);
});

app.get('/register', (req, res) => {
  res.send('中间件 get-register---' + req.time);
});

app.post('/register', (req, res) => {
  res.send('中间件 post-register---' + req.time);
});
