const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let timeStamp = require('time-stamp'); // 引入时间模块

app.get('/login', (req, res) => {
  res.send('get-login---' + timeStamp('YYYY年MM月DD日 HH:mm:ss'));
});

app.post('/login', (req, res) => {
  res.send('post-login---' + timeStamp('YYYY年MM月DD日 HH:mm:ss'));
});

app.get('/register', (req, res) => {
  res.send('get-register---' + timeStamp('YYYY年MM月DD日 HH:mm:ss'));
});

app.post('/register', (req, res) => {
  res.send('post-register---' + timeStamp('YYYY年MM月DD日 HH:mm:ss'));
});
