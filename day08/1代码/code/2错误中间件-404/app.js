const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});
const path = require('path');

app.get('/demo1', (req, res) => {
  res.send('demo1');
});

app.get('/demo2', (req, res) => {
  res.send('demo2');
});

// 404中间件
// 错误中间件一般不加前缀，也不用next下一步，放在所有路由的最后面，要设置状态码
// 返回json和返回页面任选一种（根据项目需求）
app.use((req, res) => {
  // 1、返回json数据给前端工程师看
  // res.status(404); // 设置状态码
  // res.send({
  //   code: 404,
  //   msg: 'not found',
  // });

  // 2、返回一个404页面，给用户看
  res.status(404); // 设置状态码
  let filePath = path.join(__dirname, './views/404.html');
  res.sendFile(filePath);
});
