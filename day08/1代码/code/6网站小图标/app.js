const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 引入模块
var favicon = require('serve-favicon');
var path = require('path');

// 使用网站小图标
app.use(favicon(path.join(__dirname, 'public', 'icon.ico')));

app.get('/demo', (req, res) => {
  res.send('<h1>你好</h1>');
});
