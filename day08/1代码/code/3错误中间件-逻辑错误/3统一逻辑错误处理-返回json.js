const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 普通路由可以有第三个参数next，next调用并传参数，就会到统一逻辑错误处理中，参数给到err
app.get('/register', (req, res, next) => {
  let { user, pass } = req.query;

  // 1、非空判断
  if (!user || !pass) {
    next('用户名和密码不能为空');
    return;
  }

  // 2、用户名是否被占用。如果user是admin，就说已占用
  if (user === 'admin') {
    next('用户名已被占用');
  } else {
    res.send({
      code: 200,
      msg: '注册成功',
    });
  }
});

// 统一逻辑错误处理，写在最下面，不加前缀，它的函数有四个参数，第一个参数err为传过来的错误信息
app.use((err, req, res, next) => {
  res.status(500); // 逻辑错误状态码应该是公司规定的，不同的错误，有相应的状态码，后端只要看到状态码，就可以确定错误类型
  res.send({
    code: 500,
    msg: err,
  });
});
