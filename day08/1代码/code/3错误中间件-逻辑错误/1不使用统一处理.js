const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 如果要修改，比较繁锁
app.get('/register', (req, res) => {
  let { user, pass } = req.query;

  // 1、非空判断
  if (!user || !pass) {
    res.send('<h1>用户名和密码不能为空</h1>');
    return;
  }

  // 2、用户名是否被占用。如果user是admin，就说已占用
  if (user === 'admin') {
    res.send('<h1>用户名已被占用</h1>');
  } else {
    res.send('<p>注册成功</p>');
  }
});
