const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

var svgCaptcha = require('svg-captcha'); // 引入验证码模块

// 当访问 /captcha 路由时，就会有验证码，它做为图片的src地址去使用
app.get('/captcha', function (req, res) {
  // var captcha = svgCaptcha.create(); // 创建时没有传参，生成基本配置的验证码，返回一个对象

  // 有参数
  var captcha = svgCaptcha.create({
    size: 4, // 验证码长度
    ignoreChars: '0oO1iIlL', // 排除哪些字符
    noise: 3, // 干扰线条数
    color: true, // 是否启用彩色
    // background: '#cc9966', // 背景色
  });

  console.log(captcha.text); // 对象中有验证码的文本内容

  res.type('svg');
  res.status(200);
  res.send(captcha.data); // 对象中有验证码图片
});
