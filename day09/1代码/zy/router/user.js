const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const timeStamp = require('time-stamp');

// 响应login页面
router.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/login.html'));
});

// 响应register页面
router.get('/register', (req, res) => {
  res.sendFile(path.join(__dirname, '../views/register.html'));
});

// login功能
router.post('/login', (req, res, next) => {
  let { tel: username, pass: password } = req.body; // 获取参数
  // 非空判断
  if (!username || !password) {
    next('手机号和密码必须填写');
    return;
  }

  // 读取本地json
  let dataUrl = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(dataUrl, 'utf-8');
  data = JSON.parse(data);

  // 逻辑检查
  let o = data.find(
    (item) => item.username === username && item.password === password
  );
  if (o) {
    res.send({
      code: 200,
      msg: '登录成功',
    });
  } else {
    next('用户和密码不匹配');
  }
});

// register功能
router.post('/register', (req, res, next) => {
  let { tel: username, pass: password, pass2 } = req.body; // 获取参数

  // 非空判断
  if (!username || !password || !pass2) {
    next('手机号和密码必须填写');
    return;
  }
  if (password !== pass2) {
    next('两次密码必须一致');
    return;
  }

  // 读取本地json
  let dataUrl = path.join(__dirname, '../data/persons.json');
  let data = fs.readFileSync(dataUrl, 'utf-8');
  data = JSON.parse(data);

  // 逻辑检查
  let index = data.findIndex((item) => item.username === username);
  if (index === -1) {
    // 可以注册
    data.push({
      username,
      password,
      time: timeStamp('YYYY年MM月DD日 HH:mm:ss'),
    });
    fs.writeFileSync(dataUrl, JSON.stringify(data));

    res.send({
      code: 200,
      msg: '注册成功',
    });
  } else {
    // 用户名已占用
    next('用户名已占用');
  }
});

module.exports = router;
