const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');

// 开放静态
app.use('/static', express.static(path.join(__dirname, './static')));

// post请求中间件
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// 引入模块化路由
let user = require('./router/user');
app.use(user);

// 404错误
app.use((req, res) => {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

// 逻辑错误
app.use((err, req, res, next) => {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});
