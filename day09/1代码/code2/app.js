var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

// 引入模块化路由
var indexRouter = require('./routes/index');

var app = express(); // 创建服务

// ejs
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// 接收post参数
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// cookie
app.use(cookieParser());
// 开启静态资源
app.use(express.static(path.join(__dirname, 'public')));

// 使用路由
app.use('/', indexRouter); // 路由没有加前缀

app.use(function (req, res, next) {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

app.use(function (err, req, res, next) {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});

module.exports = app;
