const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const path = require('path');
var svgCaptcha = require('svg-captcha'); // 引入验证码
const cookieSession = require('cookie-session'); // 引入cookieSession中间件
// 使用cookieSession中间件
app.use(
  cookieSession({
    keys: ['dsafsadfsda', 'dsfsewr', 'sfdsaf'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

// 验证码路由，访问/captcha 时，可以得到验证码图片
app.get('/captcha', function (req, res) {
  var captcha = svgCaptcha.create({
    // size: 4, // 验证码长度，默认4个
    ignoreChars: '0o1i', // 忽略哪些字符串
    noise: 3, // 干扰线的条数
    // background:'#999999', // 背景色
    // color: true, // 文字是否加颜色（没有背景色时才起效）
  });

  // console.log(captcha.text); // 验证码文本内容，它要存在session中
  req.session.captcha = captcha.text; // 验证码设置到session中（关键）

  res.type('svg'); // svg格式的文件类型，它是一个图片
  res.status(200);
  res.send(captcha.data); // data属性就是前端要用的图片数据
});

// 登录页面
app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, './views/login.html'));
});

// 登录功能
app.get('/dologin', (req, res) => {
  // console.log(req.query); // 取得用户输入的验证码
  // console.log(req.session.captcha); // 在session中取得验证码

  if (req.query.code.toLowerCase() === req.session.captcha.toLowerCase()) {
    res.send('验证成功');
  } else {
    res.send('验证失败');
  }
});
