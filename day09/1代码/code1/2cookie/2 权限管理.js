const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieParser = require('cookie-parser'); // 引入cookieparser中间件
app.use(cookieParser()); // 使用cookie-parser中间件，全局开启cookie功用

// 登录页面设置cookie
app.get('/login', (req, res) => {
  // 判断用户名和密码，通过后设置
  res.cookie('username', '隔壁老王', { maxAge: 24 * 60 * 60 * 1000 });

  res.send('登录成功');
});

// 欢迎界面
app.get('/welcome', (req, res) => {
  // 检查cookie是否存在，如果存在，显示欢迎谁，否则显示游客
  let { username } = req.cookies;
  if (username) {
    res.send('欢迎' + username + '回家');
  } else {
    res.send('欢迎游客');
  }
});

// 用户信息编缉
app.get('/user/edit', (req, res) => {
  let { username } = req.cookies;
  if (username) {
    res.send('可以编缉');
  } else {
    res.send('请登录');
  }
});
