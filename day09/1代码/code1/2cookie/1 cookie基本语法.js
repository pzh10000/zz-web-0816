const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieParser = require('cookie-parser'); // 引入cookieparser中间件
app.use(cookieParser()); // 使用cookie-parser中间件，全局开启cookie功用

// 设置cookie
app.get('/login', (req, res) => {
  // 验证浏览器传过来的用户名和密码，验证通过，才设置cookie
  // 语法：res.cookie(key, value [,option])

  res.cookie('name', 'zs'); // 会话过期，浏览器关闭，cookie消失
  res.cookie('age', 3, { maxAge: 24 * 60 * 60 * 1000 });
  res.cookie('sex', '男', { maxAge: 24 * 60 * 60 * 1000 });

  res.send('我是login，我用于设置cookie');
});

// 获取cookie
app.get('/demo', (req, res) => {
  // console.log(req.cookies); // 取得cookie，一个对象
  let { name, age, sex } = req.cookies;
  console.log(name, age, sex);

  res.send('获取cookie');
});

// 这个项目的其它页面，均可以获取cookie
app.get('/a/b/c', (req, res) => {
  console.log(req.cookies);
  res.send('其它页面获取cookie');
});
