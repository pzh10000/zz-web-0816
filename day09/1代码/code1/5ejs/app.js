const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.set('view engine', 'ejs'); // 使用ejs

// 定义数据，在ejs中使用
let username = '孙悟空';
let age = 18;
let sex = '男';
let isPerson = false;
let ah = ['吃香蕉', '偷蟠桃', '定七仙女'];

app.get('/index', (req, res) => {
  // 使用ejs模板，并传入对象参数
  res.render('index', { username, age, sex, isPerson, ah });
});

app.get('/user', (req, res) => {
  res.render('user');
});
