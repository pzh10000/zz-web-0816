const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

const cookieSession = require('cookie-session'); // 引入cookieSession中间件
app.use(
  cookieSession({
    // name: 'pzh', // sessionid的名称，默认可以省略
    keys: ['sfdsfkds;', 'sdfsadfsyut', 'retgretyre'], // 密钥锁，随便加，不限个数
    maxAge: 24 * 60 * 60 * 1000, // ms，session的有效期
  })
);

// 设置session
app.get('/demo1', (req, res) => {
  // 设置
  // req.session.key = value
  req.session.name = '老王';
  req.session.age = 3;
  req.session.obj = {
    a: 1,
    b: 2,
  };
  res.send('设置session');
});

// 获取session
app.get('/demo2', (req, res) => {
  // 获取
  // req.session 返回一个对象
  console.log(req.session);
  res.send('获取session');
});
