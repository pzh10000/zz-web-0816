const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});
const fs = require('fs');
const path = require('path');
const formidable = require('formidable'); // 引入文件上传模块

// 渲染上传页面
app.get('/upload', (req, res) => {
  res.sendFile(path.join(__dirname, './up.html'));
});

// 上传功能
app.post('/up', (req, res) => {
  // 实例化一个上传表单功能
  const form = formidable({
    uploadDir: path.join(__dirname, './tempdir'), // 临时的路径存放地址，要绝对地址
  });

  form.parse(req, (err, fields, files) => {
    // err: 错误信息
    // fields : 普通的表单元素数据
    // files : 文件数据
    // 处理具体的业务。比如：文件名的重命名，把文件最终放在的位置是哪里
    // console.log(err); // 错误信息
    // console.log(fields); // 普通的表单元素数据
    // console.log(files); // 文件数据

    // 方式一:上传不改名(保留原名)
    // fs.renameSync( oldpath, newpath )
    let oldpath = files.f.path; // 原路径(临时文件夹的路径)
    let newpath = path.join(__dirname, './uploads', files.f.name); // 存放路径
    fs.renameSync(oldpath, newpath);

    if (!err) {
      res.send('上传成功');
    } else {
      res.send('上传失败');
    }
  });
});
