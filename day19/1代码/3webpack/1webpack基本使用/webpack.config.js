const path = require('path');

// webpack的配置
module.exports = {
  // 入口
  entry: './src/main.js', // 入口文件，这个文件中引入别的js、css、less、字体图标等
  // 出口
  output: {
    path: path.join(__dirname, './dist'), // 出口目录，必须是绝对路径
    filename: 'bundle.js', // 打包以后的文件名
  },
  // 模式
  mode: 'development', // development开发环境  production生产环境
};
