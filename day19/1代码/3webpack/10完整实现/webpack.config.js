const path = require('path');

// 引入分离css模块
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// 引入生成html的模块
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 引入处理html中图片的模块
const CopyWebpackPlugin = require('copy-webpack-plugin');

// webpack的配置
module.exports = {
  context: path.join(__dirname, './src'), // 配置入口路径（不是必须的），以后的相对路径都相对于这个文件夹
  // 入口
  entry: './main.js', // 入口文件，这个文件中引入别的js、css、less、字体图标等
  // 出口
  output: {
    path: path.join(__dirname, './dist'), // 出口目录，必须是绝对路径
    filename: 'bundle.js', // 打包以后的文件名
  },
  // 模式
  mode: 'development', // development开发环境  production生产环境
  module: {
    rules: [
      // 加载器的使用规则
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          // 从一个已存在的 loader 中，创建一个提取(extract) loader。
          fallback: 'style-loader',
          use: 'css-loader', // loader被用于将资源转换成一个CSS单独文件
        }),
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'less-loader'],
        }),
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/, // 处理图片文件
        use: 'url-loader',
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/, // 处理字体图标
        loader: 'url-loader',
      },
    ],
  },
  plugins: [
    // 插件的配置，插件需要new
    new ExtractTextPlugin('./css/style.css'), // 这个参数即为分离后的css放到哪里，放到这个打包以后的dist/css/style.css，在页面中需要link引入一下

    new HtmlWebpackPlugin({
      // 插件配置对象
      title: 'webpack 的使用',
      filename: 'index.html', // 产出文件名(在dist目录查看)
      template: __dirname + '/index.html', // 以此文件来作为基准编译(注意绝对路径, 因为此文件不在src下)
      favicon: './assets/favicon.ico', // 插入打包后的favicon图标，原文件在src下面
      minify: {
        // 对html文件进行压缩，
        collapseBooleanAttributes: true, // 是否简写boolean格式的属性如：disabled="disabled"简写为disabled
        minifyCSS: true, // 是否压缩html里的css（使用clean-css进行的压缩） 默认值false
        minifyJS: true, // 是否压缩html里的js（使用uglify-js进行的压缩）
        removeAttributeQuotes: true, // 是否移除属性的引号 默认false
        removeComments: true, // 是否移除注释 默认false
        removeCommentsFromCDATA: true, // 从脚本和样式删除的注释, 默认false
      },
    }), // 数组元素是插件的new对象

    new CopyWebpackPlugin([
      {
        from: __dirname + '/src/static', // 从哪个文件夹开始复制
        to: __dirname + '/dist/static', // 复制到那个文件夹
      },
    ]),
  ],

  devServer: {
    // 在这里可以对webpack-dev-server进行一些配置
    port: 9090, // 修改端口号，默认为8080
    open: true, // 自动调用默认浏览器打开
  },
};
