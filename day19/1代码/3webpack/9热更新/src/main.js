// 引入js
import { a, b } from './js/a.js';
console.log(a, b);

// 引入css
import './css/base.css';

// 引入less
import './less/ab.less';

// 引入字体图标
import './css/myfont/iconfont.css';

// 引入jq
import $ from 'jquery';
$('img').click(function () {
  alert('哥们，点我了');
});
