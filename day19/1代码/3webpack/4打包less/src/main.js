// 引入js
import { a, b } from './js/a.js';
console.log(a, b);

// 引入css
import './css/base.css';

// 引入less
import './less/ab.less';
