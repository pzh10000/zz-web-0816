// 方式一
import { name, age, fn } from './a.js';
console.log(name);
console.log(age);
fn();

// -----------------------
// 方式二
let a3 = '小王';

// 暴露过来的数据，如果名称已占用，可以用as改名
import { a1, a2, a3 as aa3 } from './b.js';
console.log(a1, a2, a3, aa3);

// -----------------------------
// 方式三，引入默认暴露，句字可以随意起，不加大括号解构
// 默认暴露要放在前面
import getRandom, { ab, cc } from './c.js';

console.log(getRandom(20, 30));

console.log(ab, cc);
