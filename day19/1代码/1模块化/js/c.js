// 方式三：默认暴露
export default function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// 有默认的同时，还可以有单个暴露
export let ab = '小石头';
export let cc = '大石头';
