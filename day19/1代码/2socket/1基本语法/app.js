const express = require('express');
const app = express();
let server = app.listen(3000); // 端口开启成功之后，会返回一个http服务器对象

let io = require('socket.io')(server); // 引入socket.io，它返回一个函数，我们调用函数并传入http服务器，返回io服务器对象

// io对象监听 connect 连接事件
io.on('connect', (socket) => {
  // socket代表连接进来的哪个用户   io.sockets 为所有的客户端

  console.log('有一个连接进来了'); // 只要有前端连接，就会触发connect方法

  // 后端监听前端的请求
  socket.on('qth', (msg) => {
    console.log(msg); // 前端传过来的数据

    setTimeout(function () {
      // 后端向前端发起请求
      socket.emit('htq', '哥们，我是后端，我给你一颗糖吧');
    }, 1000);
  });
});
