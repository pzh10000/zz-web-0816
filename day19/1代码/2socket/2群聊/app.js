const express = require('express');
const app = express();
let server = app.listen(3000); // 端口开启成功之后，会返回一个http服务器对象
const path = require('path');

let io = require('socket.io')(server); // 引入socket.io，它返回一个函数，我们调用函数并传入http服务器，返回io服务器对象

io.on('connect', (socket) => {
  // socket代表连接进来的哪个用户   io.sockets 为所有的客户端

  // 后端监听前端的请求
  socket.on('qth', (name, msg) => {
    // console.log(name, msg);

    io.sockets.emit('htq', `${name}说：${msg}`);
  });
});

// 开放一个端口
app.get('/test', (req, res) => {
  res.sendFile(path.join(__dirname, './index.html'));
});
