const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 手动配置中间件跨域
// app.use(function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*'); // 所有域名
//   res.header('Access-Control-Allow-Methods', '*'); // 允许所有方法进行跨域
//   next();
// });

const cors = require('cors');
app.use(cors()); // 使用cors中间件，允许跨域

app.get('/demo', (req, res) => {
  // res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:5500'); // 只让这个域名可以跨域
  // res.header('Access-Control-Allow-Origin', '*'); // 所有域名

  res.send({
    code: 200,
    msg: 'get请求成功',
  });
});

app.post('/demo', (req, res) => {
  // res.header('Access-Control-Allow-Origin', '*'); // 所有域名
  res.send({
    code: 200,
    msg: 'post请求成功',
  });
});

app.delete('/demo', (req, res) => {
  res.send({
    code: 200,
    msg: 'delete请求成功',
  });
});

app.put('/demo', (req, res) => {
  res.send({
    code: 200,
    msg: 'put请求成功',
  });
});
