const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.get('/demo', (req, res) => {
  // console.log(req.query);
  let { callback } = req.query;

  res.send(`${callback}({code: 200,msg: 'jsonp跨域'})`);
});
