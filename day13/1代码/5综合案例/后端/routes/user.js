var express = require('express');
var router = express.Router();
const Query = require('../tools/db'); // 数据库模块

// 登录路由

// 注册路由
router.post('/register', async (req, res, next) => {
  // 1、获取用户输入
  let { username, password, password2 } = req.body;

  // 2、判断
  if (!username || !password || !password2) {
    next('用户名和密码必须输入');
    return;
  }
  if (password !== password2) {
    next('两次密码必须一致');
    return;
  }

  // 3、查数据库用户名是否已占用
  let sql1 = `SELECT * FROM user WHERE username='${username}'`;
  let [err1, result1] = await Query(sql1);
  if (err1) {
    next('服务内部错误');
    return;
  }
  if (result1.length) {
    next('用户名已占用');
    return;
  }

  // 4、向数据库插入一条
  let sql2 = `INSERT INTO user (username, password) VALUES ('${username}', '${password}') `;
  let [err2, result2] = await Query(sql2);
  if (err2) {
    next('服务内部错误');
    return;
  }
  if (result2.affectedRows) {
    res.send({
      code: 200,
      msg: '注册成功',
    });
  } else {
    next('服务内部错误');
    return;
  }
});

module.exports = router;
