function http({ url = '', type = 'get', data = {} }) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url,
      type,
      data,
      success(res) {
        resolve([null, res]);
      },
      error(err) {
        resolve([err]);
      },
    });
  });
}
