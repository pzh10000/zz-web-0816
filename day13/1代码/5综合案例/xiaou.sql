/*
Navicat MySQL Data Transfer

Source Server         : 王二小
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : xiaou

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2022-09-01 15:38:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
