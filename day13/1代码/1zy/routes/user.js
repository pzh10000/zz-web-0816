var express = require('express');
var router = express.Router();
const Query = require('../tools/db'); // 数据库

// 登录页面
router.get('/login', function (req, res, next) {
  res.render('login');
});

// 注册页面
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 登录功能
router.post('/login', async (req, res, next) => {
  // 1、获取用户输入
  let { username, password } = req.body;

  // 2、必要验证
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 查询数据库
  let sql = `SELECT * FROM user WHERE username='${username}' AND password = '${password}'`;
  let [err, result] = await Query(sql);
  if (err) {
    next('服务器内部错误');
    return;
  }

  // 如果返回的数组有值，则是登录成功
  if (result.length) {
    res.send({
      code: 200,
      msg: '登录成功',
    });
  } else {
    next('用户名和密码不匹配');
  }
});

// 注册功能
router.post('/register', async (req, res, next) => {
  // 1、获取用户输入
  let { username, password } = req.body;
  // console.log(username, password);

  // 2、必要验证
  if (!username || !password) {
    next('用户名和密码必须填写');
    return;
  }

  // 3、查数据库是否有这个用户
  let sql1 = `SELECT * FROM user WHERE username='${username}'`;
  let [err1, result1] = await Query(sql1);
  // console.log(err, result);
  if (err1) {
    next('服务器内部错误');
    return;
  }
  if (result1.length) {
    next('用户名已占用');
    return;
  }

  // 4、向数据库中插入一条
  let sql2 = `INSERT INTO user (username, password) VALUES ('${username}', '${password}')`;
  let [err2, result2] = await Query(sql2);
  if (err2) {
    next('服务器内部错误');
    return;
  }
  if (result2.affectedRows) {
    res.send({
      code: 200,
      msg: '注册成功',
    });
  } else {
    next('注册失败');
  }
});

module.exports = router;
