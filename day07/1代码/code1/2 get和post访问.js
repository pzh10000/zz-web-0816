const express = require('express');
const app = express();
app.listen(3000, () => {
    console.log('http://localhost:3000');
})


// 监听get请求
app.get('/register', (req, res) => {
  res.send('get请求，注册成功');
});

// 监听post请求
app.post('/register', (req, res) => {
  res.send('post请求，注册成功');
});
