const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// --------------------------
// 豆辩里面上万部电影，不可能每部电影一个路由
// app.get('/subject/35360684', (req, res) => {
//   res.send('杨戬');
// });
// app.get('/subject/35360684', (req, res) => {
//   res.send('760号犯人');
// });

// -------------------
// 动态路由
let arr = [
  { id: 23, name: '杨戬' },
  { id: 345, name: '760号犯人' },
  { id: 567, name: '今天中午吃啥' },
  { id: 24345, name: '明天吃啥' },
];

app.get('/subject/:id', (req, res) => {
  // console.log(req.query); // 取查询字符串参数
  // console.log(req.params); // 取动态路由，是一个对象
  let { id } = req.params; // 取得动态路由的参数

  let o = arr.find((item) => item.id == id);
  if (o) {
    res.send(o);
  } else {
    res.send('请检查动态路由的参数');
  }
});
