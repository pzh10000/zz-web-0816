const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 学生管理系统
// 查
app.get('/student', (req, res) => {
  res.send('获取一个学生');
});

// 增
app.post('/student', (req, res) => {
  res.send('增加一个学生');
});

// 删除
app.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

// 更新
app.put('/student', (req, res) => {
  res.send('更新一个学生');
});

// 因为上面有同样的方法和地址的路由，因此这个路由永远不会匹配
app.get('/student', (req, res) => {
  res.send('我也是获取一个学生');
});
