const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 响应多种数据类型
app.get('/demo1', (req, res) => {
  // 1、返回的数据类型更加灵活，可以是string、Object
  // 2、根据数据类型灵活的设置Content-type( **主要是html和json** )，并且默认就是utf-8
  // 2.1：普通的字符当作 html 渲染
  // 2.2 : 返回的是Object，则 application/json 渲染

  // 以下返回不用设置，这是通常用法（一次请求，只能有一次send，多次报错）
  // res.send('你好'); // 响应字符串
  // res.send('<h1>你好</h1>'); // 响应标签
  res.send({ name: 'zs', age: 3 }); // 响应对象

  // ---------------------
  // 设置响应头，此案例的目的是告诉如何设置响应头，图片不会这么访问（谷歌是下载）
  // let imgUrl = path.join(__dirname, './img/OIP-C.jpg');
  // let img = fs.readFileSync(imgUrl);
  // res.setHeader('Content-type', 'image/jpeg'); // 发送之前设置响应头
  // res.send(img);
});

app.get('/img', (req, res) => {
  // 发送一个文件，参数必须是绝对地址
  // 图片还是不这样返回，只有html页面我们这样返回
  let imgUrl = path.join(__dirname, './img/OIP-C.jpg');
  res.sendFile(imgUrl); // 专门返回html页面
});

// 返回html页面
app.get('/register', (req, res) => {
  let url = path.join(__dirname, './views/form表单发起get和post请求.html');
  res.sendFile(url); // sendFile的正确使用，返回一个html文件
});
