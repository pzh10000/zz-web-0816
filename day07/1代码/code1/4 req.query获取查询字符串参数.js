const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

app.get('/demo', (req, res) => {
  // console.log(req.query); // 获取查询字符串参数，返回一个对象
  let { username, pass } = req.query; // 解构

  res.send(`我的名字是${username}，我的密码是${pass}`);
});
