// 引包
const express = require('express');
// console.log(express);

// 创建服务
let app = express();

// 监听端口，第二个回调函数可有可无，方便开发人员看的
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 访问根目录 首页    http://localhost:3000/
app.get('/', (req, res) => {
  res.send('首页');
});

// 访问登录    http://localhost:3000/login
app.get('/login', (req, res) => {
  res.send('<h1>登录页面</h1>');
});

// 访问注册   http://localhost:3000/register
app.get('/register', (req, res) => {
  res.send({ name: 'zs', age: 3 });
});
