const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 学生类
app.get('/student', (req, res) => {
  res.send('获取一个学生');
});

app.post('/student', (req, res) => {
  res.send('添加一个学生');
});

app.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

app.put('/student', (req, res) => {
  res.send('更新一个学生');
});

// 老师类
app.get('/teacher', (req, res) => {
  res.send('获取一个老师');
});

app.post('/teacher', (req, res) => {
  res.send('添加一个老师');
});

app.delete('/teacher', (req, res) => {
  res.send('删除一个老师');
});

app.put('/teacher', (req, res) => {
  res.send('更新一个老师');
});

// 教室类

// 电脑类
