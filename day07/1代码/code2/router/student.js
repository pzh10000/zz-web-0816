// 1、引入express，并创建路由模块
const express = require('express');
const router = express.Router();

// 2、定义路由
// 学生类
router.get('/student', (req, res) => {
  res.send('获取一个学生');
});

router.post('/student', (req, res) => {
  res.send('添加一个学生');
});

router.delete('/student', (req, res) => {
  res.send('删除一个学生');
});

router.put('/student', (req, res) => {
  res.send('更新一个学生');
});

// 3、暴露出去
module.exports = router;
