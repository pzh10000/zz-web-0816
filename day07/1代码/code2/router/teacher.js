// 1、引入express，并创建路由模块
const express = require('express');
const router = express.Router();

// 2、定义路由
// 老师类
router.get('/teacher', (req, res) => {
  res.send('获取一个老师');
});

router.post('/teacher', (req, res) => {
  res.send('添加一个老师');
});

router.delete('/teacher', (req, res) => {
  res.send('删除一个老师');
});

router.put('/teacher', (req, res) => {
  res.send('更新一个老师');
});

// 3、暴露出去
module.exports = router;
