const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

let student = require('./router/student'); // 引入路由
app.use(student); // 使用路由

let teacher = require('./router/teacher');
app.use(teacher);
