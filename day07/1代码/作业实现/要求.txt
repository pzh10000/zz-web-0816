基础作业：
    已知data/persons.json文件，存储的用户信息数据

    1、创建register.html
        注册表单（用户名name="username"和密码name="password"）框提交到http://localhost:3000/register
    2、使用http模块创建服务器
        端口开放3000
        接收传递的username和password参数
        根据之前所学注册原理， 在  /register 实现注册，并给浏览器响应提示信息

扩展作业： 
    根据登录原理，在 /login 实现登录，并给浏览器响应提示信息
