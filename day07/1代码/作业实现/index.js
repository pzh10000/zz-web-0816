// 这里实现后端逻辑

const http = require('http');
const qs = require('querystring');
const fs = require('fs');
const path = require('path');

let app = http.createServer(); // 创建服务

// 监听端口
app.listen(3000, () => {
  console.log('http://localhost:3000');
});

// 响应请求
app.on('request', (req, res) => {
  // 设置响应头信息
  res.writeHead(200, { 'Content-Type': 'text/html;charset=utf8' });

  let url = req.url; // 获取用户请求地址   /register?username=zs&passowrd=111
  let user = qs.parse(url.split('?')[1]); // 获取用户传过来的参数
  // 对参数进行空的判断
  if (!user.username || !user.password) {
    res.end('请输入用户名和密码');
    return;
  }

  let filePath = path.join(__dirname, './data/persons.json');
  let dataArr = JSON.parse(fs.readFileSync(filePath, 'utf-8')); // 获取本地json中的用户信息

  // console.log(dataArr);
  // console.log(user);

  if (url.startsWith('/register')) {
    // 注册
    let index = dataArr.findIndex((item) => item.username === user.username);
    if (index === -1) {
      // 可以注册
      dataArr.push(user);
      fs.writeFileSync(filePath, JSON.stringify(dataArr)); // 将注册信息写入json文件中
      res.end('注册成功');
    } else {
      // 用户名已占用
      res.end('用户名太火了，换一个吧');
    }
  } else if (url.startsWith('/login')) {
    // 登录
    let o = dataArr.find(
      (item) =>
        item.username === user.username && item.password === user.password
    );
    if (o) {
      res.end('登录成功');
    } else {
      res.end('用户名或密码错误');
    }
  } else {
    // 404
    res.writeHead(404, { 'Content-Type': 'text/html;charset=utf8' });
    res.end('404');
  }
});
