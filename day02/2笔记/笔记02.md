# ECMASCript6简介

ECMAScript 6.0（以下简称 ES6）泛指 JavaScript 语言的下一代标准。



## 1. Javascript遗留问题（es5之前的一些问题）

弱类型语法造成的程序不可预测性（比如：未声明变量即可使用）

语法过于松散，实现相同的功能，不同的人可能会写出不同的代码



## 2. 为什么使用ES6

es6的语法和新增一些函数，帮我们解决了一些问题，和提升一些开发效率 。

为后面阶段学习框架做准备



## 3.ECMAScript6历史版本

ES6 实际上是一个泛指，泛指 ES2015 及后续的版本

![1-1ECMA版本](img/1-1ECMA版本.png)

es6全称：es2015年6月份发布的，每年的6月份都会更新一代。



# let和const命令

ES6新增了`let`命令，用来声明变量。

ES6新增了`const`命令，用来声明一个只读的常量。



## 1.let命令

### 1.1 语法

```js
let 变量名 = 值
```



### 1.2 let命令特点

- 不存在变量提升与暂时性死区（永远是先声明，再使用）
- 不能重复声明
- 块级作用域（if for while switch function，不包括对象）
- let和循环（循环后的i和打出下标）

```js
// - 不存在变量提升与暂时性死区（永远是先声明，再使用）
console.log(a); // Cannot access 'a' before initialization
let a = 5;

// ---------------------
// - 不能重复声明
var a = 5;
let a = 6; // Identifier 'a' has already been declared
console.log(a);

// --------------
// - 块级作用域（if for while switch function，不包括对象）
// 之前作用域：全局域 和 局部域（函数域）
if (true) {
    // 这个大括号是一个块级作用域
    let a = 5;
}
console.log(a); // a is not defined

// --------------------
// - let和循环（循环后的i和打出下标）
for (var i = 0; i < 3; i++) {}
console.log(i); // 3

for (let i = 0; i < 3; i++) {}
console.log(i); // i is not defined

let li = document.querySelectorAll('li');
for (let i = 0; i < li.length; i++) {
    li[i].onclick = function () {
        console.log(i);
    };
}
```





## 2.const命令

### 2.1 语法

```js
const 常量名 = 常量值
```



### 2.2 const特点及建议

- 按照规范建议常量名大写

- 声明常量必须赋值

- 一旦声明常量，则值是不能改的（指的是内存地址不能修改）

```js
// - 按照规范建议常量名大写
const NAME = '小王';

// ------------------------

// - 声明常量必须赋值
const ABC; // Missing initializer in const declaration

// ------------------------
// - 一旦声明常量，则值是不能改的（指的是内存地址不能修改）
const ABC = 'ABC';
ABC = '213'; // Assignment to constant variable


const OBJ = {
    name: 'zs',
    age: 3,
};
OBJ.name = 'ls'; // 修改属性，内存地址并没有变
// OBJ = {}; // 如果直接赋值，则是修改内存地址
console.log(OBJ);
```






# 数据类型和数据结构

# 一、字符串扩展

## 1.1 模板字符串

原字符串的问题：

- 字符串拼接复杂
- 引号嵌套的问题
- 定义字符串时不能换行



### 语法

```js
let str = `中公优就业 IT 培训品牌`
```

可以当作普通字符串使用，也可以用来定义多行字符串，或者在字符串中嵌入变量



### 优点

可以解析 js 表达式（变量、字符串拼接、函数调用、三目运算）

```js
${简单的js表达式}
```

```js
// 作用：可以当作普通字符串使用，也可以用来定义多行字符串，或者在字符串中嵌入变量
// 优点：可以解析 js 表达式（变量、字符串拼接、函数调用、三目运算）
// ${js表达式}

let h1 = document.querySelector('h1');

let uname = '平头哥';
let from = function () {
    return '大自然';
};
let sex = 0;
let age = 12;

h1.innerHTML = `我是${uname}，我来自于${from()}，我是${
sex === 0 ? '男' : '女'
}孩子，我明年${age + 1}岁`;
```



## 1.2 新增方法（5个）

- startsWith

start（开始）

语法：字符串.startsWith( 参数字符 )

描述：参数字符串是否在原字符串的头部，返回布尔值   （判断路径以什么开头）

- endsWith 

语法：字符串.endsWith( 参数字符 )

描述：参数字符串是否在原字符串的尾部，返回布尔值  （判断图片扩展名）

-  repeat

语法：字符串.repeat( num )

描述：返回一个新字符串，表示将原字符串重复num次

- padStart

语法：字符串.padStart( 目标长度，填充的字符 )

描述：字符串头部补全（如果字符串没有达到这个长度，则在前面添加字符，以达到这个长度）（如补0）

-  padEnd

语法：字符串.padEnd( 目标长度，填充的字符 )

描述：字符串尾部补全（如果字符串没有达到这个长度，则在尾部添加字符，以达到这个长度）



```js
// startsWith
// 语法：字符串.startsWith( 参数字符 )
// 描述：参数字符串是否在原字符串的头部，返回布尔值   （判断路径以什么开头）
let str = '/home/ab.html';
console.log(str.startsWith('/home')); // true
console.log(str.startsWith('/home2')); // false

// -------------------------
// endsWith
// 语法：字符串.endsWith( 参数字符 )
// 描述：参数字符串是否在原字符串的尾部，返回布尔值  （判断图片扩展名）
let imgName = 'ab.jpg';
console.log(imgName.endsWith('.jpg')); // true
console.log(imgName.endsWith('.png')); // false

// ----------------------
// repeat
// 语法：字符串.repeat( num )
// 描述：返回一个新字符串，表示将原字符串重复num次
let str = '田文辉...';
let s = str.repeat(5);
console.log(s);

// -----------------
// padStart
// 语法：字符串.padStart( 目标长度，填充的字符 )
// 描述：字符串头部补全（如果字符串没有达到这个长度，则在前面添加字符，以达到这个长度）（如补0）
let str = '你爱我';
let s = str.padStart(10, '你');
console.log(s); // 你你你你你你你你爱我

console.log('7'.padStart(2, '0'));
console.log('17'.padStart(2, '0'));

// --------------------------
// padEnd
// 语法：字符串.padEnd( 目标长度，填充的字符 )
// 描述：字符串尾部补全（如果字符串没有达到这个长度，则在尾部添加字符，以达到这个长度）
let str = '你爱我';
console.log(str.padEnd(10, '我')); // 你爱我我我我我我我我
```







# 二、Symbol

做为了解即可，用途比较少。为了我们后续学习iterator做铺垫。Symbol 可以当作对象的键。

ES6 引入了一种新的原始数据类型`Symbol`，表示独**一无二的值**。

它是 JavaScript 语言的第七种数据类型。 

Symbol 值通过`Symbol()`函数生成。

## 1. 基础用法

代码案例：

```js
// 1、基本用法（定义一个symbol的值）
let sym = Symbol();
console.log(sym);
console.log(typeof sym); // 'symbol'

// 2、symbol不做任何运算，会报错
console.log(sym + 1);

// 3、独一无二
let sym2 = Symbol();
console.log(sym == sym2); // false
```



## 2. Symbol描述符

```js
// 标识符，就是为了好看，好区分是哪一个symbol
let sym1 = Symbol('name');

let sym2 = Symbol('age');

console.log(sym1);
console.log(sym2);
```



## 3. 应用场景

Symbol可以用在对象中，当作键使用。传统的键是使用字符串的，在这里可以使用symbol。

一般常用于框架、js内置函数和对象中

代码案例：

```js
let sym1 = Symbol('name');
let sym2 = Symbol('age');
let obj = {
    [sym1]: 'zs',
    [sym2]: 3,
};
console.log(obj); // {Symbol(name): 'zs', Symbol(age): 3}
console.log(obj[sym1]); // zs
```

扩展案例（模拟对象的私有属性）：

```js
function fn(name, age) {
    let sym1 = Symbol('name');
    let sym2 = Symbol('age');

    return {
        [sym1]: name,
        [sym2]: age,
    };
}

let o = fn('zs', 3); // {Symbol(name): 'zs', Symbol(age): 3}
console.log(o); // 访问不到name和age，因此说它们是对象的私有属性
```



# 三、数组

es6中对数组一些方法进行了扩展。**数组这种数据结构的特点：有序的。**

## 1. 新增方法（静态方法）

静态方法：方法定义在构造函数下面   (Array.isArray()    )

实例方法：方法定义在原型上

```js
function Array(){}

Array.abc = function (){} // 静态方法
Array.prototype.cde = function (){} // 实例方法
```





-  **Array.of**

语法：Array.of( ...args )

作用：实例化数组，解决new Array()传入数字时的缺陷。

代码案例：

```js
let arr1 = [3];
let arr2 = new Array(3); // 产生歧义
console.log(arr1); // [3]
console.log(arr2); // [空属性 × 3]

let arr3 = Array.of(3);
console.log(arr3); // [3]
```

- **Array.from**

语法：Array.from( arrayLike )

作用：把伪数组转换成数组，相对于ES5的写法，更加简洁方便

伪数组有：arguments、NodeList、HtmlCollection 

代码案例：

```js
let li = document.querySelectorAll('li');
console.log(li); // 伪数组

// 之前的方式转为数组
let arr = [];
for (let i = 0; i < li.length; i++) {
    arr.push(li[i]);
}
console.log(arr);

// es6的方式
console.log(Array.from(li));
```



# 四、对象

对象（object）是 JavaScript 最重要的数据类型。ES6 对它进行了重大升级，包括（数据结构本身的改变和Object对象的新增方法）

特点：**对象是无序的**

## 1. 对象属性简写/函数的简写

代码案例：

```js
let uname = 'zs';
let age = 3;

// let obj = {
//   uname: uname,
//   age: age,
// };

let obj = {
    uname, // 属性的简写，如果key和value刚好一样，我们就可以简写
    age,
    fn: function () {
        console.log(123);
    },

    // 方法的简写，去掉  :function
    abc() {
        console.log(456);
    },
};

console.log(obj);
```



## 2. 属性名表达式

即对象属性名的中括号的用法：中括号中可以解析表达式（变量、字符串拼接、函数调用、三目等等）

代码案例：

```js
let name = 'zs';
let fn = function () {
    return '小二';
};
let isShangxue = true;
let ab = 'ab';

// 即对象属性名的中括号的用法：中括号中可以解析表达式（变量、字符串拼接、函数调用、三目等等）
let obj = {
    [name]: 'zs',
    [fn()]: '小二',
    [isShangxue ? 'yes' : 'no']: '上学',
    [ab + 1]: 'ab',
};
console.log(obj);

// -----------------------------
// 定义一个对象，它的key是a1 --- a100
let o = {};
for (let i = 1; i <= 100; i++) {
    o['a' + i] = i;
}
console.log(o);
```





## 3. 新增静态方法

-  **Object.assign**

语法： Object.assign( target, source1, source2, ... )

描述：对象的合并( 浅拷贝 )，将源对象（source），复制到目标对象 ( target )，并返回目标对象

代码案例：

```js
// 类似于$.extend()。但是它没有$.extend()强大，因为assign只能浅克隆

let o1 = {
    name: 'zs',
};
let o2 = {
    age: 3,
    sex: '男',
};
let o3 = {
    name: 'ls',
};
let o = Object.assign({}, o1, o2, o3);
console.log(o); // {name: 'ls', age: 3, sex: '男'}
```



- **Object.keys**

语法：Object.keys( object )

描述：返回一个数组，成员是参数对象自身的属性的键名

代码案例：

```js
let obj = {
    name: 'zs',
    age: 3,
    fn() {
        console.log(123);
    },
};

// 语法：Object.keys( object )
// 描述：返回一个数组，成员是参数对象自身的属性的键名
console.log(Object.keys(obj)); // ['name', 'age', 'fn']
```



- **Object.values**

语法：Object.values( object )

描述：返回一个数组，成员是参数对象自身的属性的键值

代码案例：

```js
let obj = {
    name: 'zs',
    age: 3,
    fn() {
        console.log(123);
    },
};

// 语法：Object.values( object )
// 描述：返回一个数组，成员是参数对象自身的属性的键值
console.log(Object.values(obj)); // ['zs', 3, ƒ]
```



案例 

```js
// 使用：循环对象，利用Object.keys将对象像数组一样循环
Object.keys(obj).forEach(function (item) {
    console.log(item, obj[item]);
});
```



## 4.JSON

- **JSON.parse**

语法：JSON.parse( jsonstr)

描述：把json字符串转换成js对象

**json字符串: 数据类型一定是字符串** **'{}'  且json字符串中的属性名必须是双引号，如果值为字符串则字符串也必须是双引号**



- **JSON.stringify**

语法：JSON.stringify( object )

描述：把js对象转换成json字符串

代码案例：

```js
let obj = {
    name: 'zs',
    age: 3,
};

// 语法：JSON.stringify( object )
// 描述：把js对象转换成json字符串
let str = JSON.stringify(obj);
console.log(str); // {"name":"zs","age":3}

// 语法：JSON.parse( jsonstr)
// 描述：把json字符串转换成js对象
console.log(JSON.parse(str)); // {name: 'zs', age: 3}

// --------------------
// 深拷贝
let o = JSON.parse(JSON.stringify(obj));
console.log(o);
```

作用：对象深拷贝





# 五、Set（了解）

ES6 提供了新的数据结构 Set。**它类似于数组，但是成员的值都是唯一的，没有重复的值**。

本质上Set是一个构造函数（实例化对象：创建一个set数据结构）

## 1. 基本用法

```js
new Set(数组 | 类数组)
```

代码案例：

```js
// 语法：new Set(数组 | 类数组)
// 特点：它类似于数组，但是成员的值都是唯一的，没有重复的值

let arr = [3, 3, 2, 3, 2, 4, 3, 5, 6];

let set = new Set(arr); // 创建set数据
console.log(set); // {3, 2, 4, 5, 6}
```



## 2. 常见方法

- **size**   获取set集合的长度
- **add(值)**  给集合添加值，返回新set
- **delete(值)**  删除某个值，返回布尔值，表示是否删除成功
- **has(值)**  查询这个值是否时集合的成员，返回布尔值
- **clear()**  清空集合,没有返回值

代码案例：

```js
// size   获取set集合的长度
console.log(set.size); // 5

// add(值)  给集合添加值，返回新set
let s = set.add('3');
console.log(s);

// delete(值)  删除某个值，返回布尔值，表示是否删除成功
let v = set.delete(3);
console.log(v); // true
console.log(set); // {2, 4, 5, 6, '3'}

// has(值)  查询这个值是否时集合的成员，返回布尔值
let n = set.has('3');
console.log(n); // true

// clear()  清空集合,没有返回值
set.clear();
console.log(set);
```

## 3. 应用

数组去重

```js
// 案例：数组去重
let arr1 = [3, 3, 2, 3, 2, 4, 3, 5, 6];
console.log(Array.from(new Set(arr1))); // [3, 2, 4, 5, 6]
```



# 六、Map（了解）

ES6 提供了 Map 数据结构。**它类似于对象**，也是键值对的集合，但是“键”的范围不限于字符串，各种类型的值（包括对象）都可以当作键。

**注意：对象的键只能是字符串或symbol，而map的键可以是任何数据类型**

本质上Map 是一个构造函数（实例化对象：创建一个map数据结构）

## 1. 基本用法

```js
new Map( [ [key1,value1],[key2,value2],... ] )
```

```js
// 语法：new Map( [ [key1,value1],[key2,value2],... ] )
let arr = ['a', 'b'];
let map = new Map([
    [null, '这是一个空'],
    [undefined, '这是未定义'],
    [3, '数字'],
    ['ab', 'ab'],
    [true, '布尔'],
    [arr, '数组'],
    [{ name: 'zs' }, '对象'],
]);
console.log(map);
```





## 2. 常见方法

- **size**  获取Map的长度
- **set(key,val)**  添加一条数据
- **get(key)**  根据key获取val
- **has(key)**  是否存在key
- **delete(key)** 删除某条数据
- **clear()** 清空所有数据

代码案例：

```js
// - size  获取Map的长度
console.log(map.size); // 7

// - set(key,val)  添加一条数据
map.set('添加一条', 'ab');
console.log(map);

// - get(key)  根据key获取val
console.log(map.get(true));

// - has(key)  是否存在key
console.log(map.has(arr)); // true

// - delete(key) 删除某条数据
let v = map.delete(arr);
console.log(v); // true
console.log(map);

// - clear() 清空所有数据
map.clear();
console.log(map);
```





# 运算符表达式



# 七、解构赋值

## 1.概念及意义

ES6 允许按照一定**模式**，从数组和对象中提取值，对变量进行赋值，这被称为解构赋值

从而更加方便地从数组或对象中取值。解构赋值分为**数组解构**和**对象解构**

## 2.数组解构

语法：

```js
let [ 变量1, 变量2, ... ] = [ value1, value2, ... ]
```

关键：因为数组是有序的，两边模式一致，则可以右边的值一一对应的赋给左边



### 2.1 完全解构( 掌握 )

代码案例：前面和后面一一对应，按照顺序依次为左边的变量赋值

```js
let [a, b, c] = arr;
console.log(a, b, c);
```



### 2.2 不完全解构

代码案例：前面比后面少

```js
let [a, b] = arr;
console.log(a, b);
```



### 2.3 解构失败

代码案例：前面多，后面少

```js
let [a, b, c, d] = arr;
console.log(a, b, c, d); // 刘备 关羽 张飞 undefined
```



### 2.4 解构默认值

没有值为其变量赋值，或者赋的值为undefined ，默认值才生效

```js
let [aa,bb,cc=123] = [11,22]
```

代码案例：

```js
let [a, b, c, d = '小田'] = arr;
console.log(a, b, c, d); // 刘备 关羽 张飞 小田
```

### 2.5 缺省( 掌握 )

```js
let [a, , c] = arr;
console.log(a, c);
```



### 2.6 数组解构应用(数据交换)

```js
// 数组解构应用(数据交换)
let num1 = 10;
let num2 = 20;

// 之前做法
let temp = num1;
num1 = num2;
num2 = temp;

// es6的做法
[num2, num1] = [num1, num2];

console.log(num1, num2);
```



## 3.对象解构

对象的解构与数组解构有一个重要的不同。数组的元素是按次序排列的，而对象的属性没有次序。只和键名有关

语法：

```js
// 完整语法
let { key2:变量名2, key1:变量名1 } = { key1:value1, key2:value2,... }
作用：把key2属性中的值赋值给变量名2，把key1属性中的值赋值给变量名1
                               
// 简写语法：
let { key2, key1 } = { key1:value1, key2:value2,... }     
简写代表把同名属性的值给了一个和属性名相同的变量（我们就不用再思考起变量名）
```



```js
let person = {
    username: 'zs',
    age: 2
}
```



### 3.1 完整语法

代码案例：

```js
let { username: name, age: age } = person;
console.log(name, age);
```



### 3.2 简写语法解构(推荐)

代码案例：

```js
let { username, age } = person;
console.log(username, age);
```



### 3.3 解构失败

代码案例：当没有对应的值的时候，就是解构失败

```js
let { username, age, sex } = person;
console.log(username, age, sex); // zs 2 undefined
```

### 3.4 解构默认值（重点）

代码案例：当值对应的是undefined时，就可以使用默认值

```js
let { username, age, sex = '男' } = person;
console.log(username, age, sex); // zs 2 男
```



### 3.5 使用场景

代码案例1：从事件对象中解构鼠标位置等信息

```js
// 从事件对象中解构鼠标位置等信息
let box = document.querySelector('#box');

box.onclick = function (ev) {
    console.log(ev.clientX);
    console.log(ev.clientY);
};

// 新的做法
box.onclick = function ({ clientX, clientY }) {
    console.log(clientX, clientY);
};
```

代码案例2：优化函数传参

如果函数需要传很多个参数，就要注意顺序。但我们可以改成只传一个对象，然后在形参哪里解构接收

```js
// 如果函数需要传很多个参数，就要注意顺序。但我们可以改成只传一个对象，然后在形参哪里解构接收

// 函数接收多个参数，就要注意传参的顺序
function fn(a, b, c, d) {
    console.log(a, b, c, d);
}
fn(1, 2, 3, 4);



// 传入一个对象，并可以设置默认值
function fn({ a, b = '2', c, d }) {
    console.log(a, b, c, d);
}

fn({ a: 1, c: 3, d: 4 });
```



# 八、...运算符

## 1. rest参数

接收剩余参数（如果没有定义形参，则接收所有参数），以数组形式接收，...rest参数必须放在最后，为了代替arguments。（用在函数形参哪里）

```js
// function fn(a, b, c, d) {
//   console.log(a, b, c, d);
// }
// fn(1, 2, 3, 4);

// -------------------
// 利用arguments接收参数
// function fn() {
//   console.log(arguments);
// }
// fn(1, 2, 3, 4);

// ----------------------
// ...rest参数，接收所有剩余的参数，
// 作用：代替arguments
function fn(...rest) {
    console.log(rest); // [1, 2, 3, 4]
}
fn(1, 2, 3, 4);

function fn(a, b, ...rest) {
    console.log(rest); // [3, 4]
}
fn(1, 2, 3, 4);
```





## 2. spread参数

rest参数的逆运算。（用在函数实参哪里）

当作实参进行传递（展开数组的数据），展开数组、展开对象（ **可用于浅拷贝、数组合并、伪数组转数组** ）

- 当作实参进行传递（展开数组的数据）
- 实现数组、对象浅拷贝
- 数组合并、对象合并
- 伪数组转数组

```js
// 1、当作实参进行传递（展开数组的数据）
function fn(a, b, c, d) {
    console.log(a, b, c, d);
}
var arr = [1, 2, 3, 4];
fn(...arr);

// 2、数组浅拷贝
let arr = ['a', 'b', 'c'];
// 数组浅拷贝：循环 concat slice
let arr1 = [...arr];
console.log(arr1);

// 3、对象浅拷贝
let obj = {
    name: 'zs',
    age: 3,
};
let obj2 = { ...obj };
console.log(obj2);

// ---------------
// 4、数组合并
let arr1 = [1, 2, 3];
let arr2 = ['a', 'b', 'c'];
let arr = [...arr1, ...arr2];
console.log(arr);

// 5、对象合并
let o1 = {
    name: 'zs',
};
let o2 = {
    age: 3,
    sex: '男',
};
let o = { ...o1, ...o2 };
console.log(o); // {name: 'zs', age: 3, sex: '男'}

// 6、伪数组转数组
let li = document.getElementsByTagName('li');
console.log(li);
console.log([...li]); // [li, li, li]
```



案例：找数组的最大值

```js
let arr = [3, 3, 3, 4, 26, 44, 3];

console.log(Math.max(3, 3, 3, 4, 26, 44, 3)); // 44
console.log(Math.max.apply(Math, arr)); // 44
console.log(Math.max(...arr)); // 44
```





案例：每个人薪水加5000

```js
var persons = [
    {
        username: '张飞',
        sex: '男',
        salary: 50000,
    },
    {
        username: '关羽',
        sex: '男',
        salary: 60000,
    },
];

let o = persons.map(function (item) {
    return {
        ...item,
        salary: item.salary + 2000,
    };
});

console.log(o);
console.log(persons);
```

