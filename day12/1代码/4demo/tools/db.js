const mysql = require('mysql');

let connectObj = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  port: 3306,
  database: 'xiaou',
});

function Query(sql = '') {
  return new Promise((resolve, reject) => {
    connectObj.query(sql, (err, result) => {
      resolve([err, result]);
    });
  });
}

module.exports = Query;
