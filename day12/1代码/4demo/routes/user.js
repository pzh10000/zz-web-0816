var express = require('express');
var router = express.Router();

// 登录页面
router.get('/login', function (req, res, next) {
  res.render('login');
});

// 注册页面
router.get('/register', function (req, res, next) {
  res.render('register');
});

// 登录功能
router.post('/login', (req, res, next) => {
  res.send('登录功能');
});

// 注册功能
router.post('/register', (req, res, next) => {
  res.send('注册功能');
});

module.exports = router;
