var express = require('express');
var router = express.Router();

// 增
router.post('/student', function (req, res, next) {
  // 1、获取用户输入
  let { username, password } = req.body;
  // console.log(username, password);

  // 2、非空判断
  if (!username || !password) {
    next('用户名和密码必须填定');
    return;
  }

  // 3、数据库判断，只要username 不是 admin，都可以注册
  if (username === 'admin') {
    next('用户名已占用');
    return;
  }

  // 4、数据库增加一条信息
  res.send({
    code: 200,
    msg: '注册成功',
    result: 123, // 返回添加成功的那个id
  });
});

// 删
router.delete('/student', function (req, res, next) {
  res.send('删除一个学生');
});

// 改
router.put('/student', function (req, res, next) {
  res.send('修改一个学生');
});

// 查
router.get('/student', function (req, res, next) {
  res.send('查找一个学生');
});

// 查一组
router.get('/students', function (req, res, next) {
  res.send('查找一组学生');
});

module.exports = router;
