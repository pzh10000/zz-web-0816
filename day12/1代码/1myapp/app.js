var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

var indexRouter = require('./routes/index');
var studentRouter = require('./routes/student');
var teacherRouter = require('./routes/teacher');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// 使用路由
app.use('/', indexRouter);
app.use('/api', studentRouter); // 路由前缀加/api，代表它是接口
app.use('/api', teacherRouter);

// 404
app.use(function (req, res, next) {
  res.status(404);
  res.send({
    code: 404,
    msg: 'not found',
  });
});

// 500
app.use(function (err, req, res, next) {
  res.status(500);
  res.send({
    code: 500,
    msg: err,
  });
});

module.exports = app;
