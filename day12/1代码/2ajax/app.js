const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
});
const path = require('path');

// 获取post参数
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// register_get
app.get('/register_get', (req, res) => {
  let url = path.join(__dirname, './views/register_get.html');
  res.sendFile(url);
});

// register_post
app.get('/register_post', (req, res) => {
  let url = path.join(__dirname, './views/register_post.html');
  res.sendFile(url);
});

// checkuser-get
app.get('/checkuser', (req, res) => {
  // 1、获取
  let { username } = req.query;

  // 2、非空判断
  if (!username) {
    res.send({
      code: 500,
      msg: '用户名必须填写',
    });
    return;
  }

  // 3、查数据，如果是admin就是已占用
  if (username === 'admin') {
    res.send({
      code: 500,
      msg: '用户名已占用',
    });
    return;
  }

  // 4、可以注册
  res.send({
    code: 200,
    msg: '注册成功',
  });
});

// checkuser-post
app.post('/checkuser', (req, res) => {
  // 1、获取
  let { username } = req.body;

  // 2、非空判断
  if (!username) {
    res.send({
      code: 500,
      msg: '用户名必须填写',
    });
    return;
  }

  // 3、查数据，如果是admin就是已占用
  if (username === 'admin') {
    res.send({
      code: 500,
      msg: '用户名已占用',
    });
    return;
  }

  // 4、可以注册
  res.send({
    code: 200,
    msg: '注册成功',
  });
});
